---
layout: markdown_page
title: "GitLab Direction"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Structure

This page describes the direction and roadmap for GitLab. It's organized from
the short to the long term.

## Your contributions

GitLab's direction is determined by GitLab the company, and the code that is
sent by our [contributors](http://contributors.gitlab.com/). We continually
merge code to be released in the next version. Contributing is the best way to
get a feature you want included.

On [our issue tracker for CE][ce-issues] and [EE][ee-issues], many requests are
made for features and changes to GitLab. Issues with the [Accepting Merge
Requests] label are pre-approved as something we're willing to add to GitLab. Of
course, before any code is merged it still has to meet our [contribution
acceptance criteria].

[ce-issues]: https://gitlab.com/gitlab-org/gitlab-ce/issues
[ee-issues]: https://gitlab.com/gitlab-org/gitlab-ee/issues
[Accepting merge requests]: https://gitlab.com/gitlab-org/gitlab-ce/issues?state=opened&label_name=Accepting+merge+requests
[contribution acceptance criteria]: https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#contribution-acceptance-criteria

## What our customers want

See our [product handbook on how we prioritize](/handbook/product/#prioritization).

## How we plan releases

At GitLab, we strive to be [ambitious](/handbook/product#ambitious-planning),
maintain a strong sense of urgency, and set aspirational targets with every
release. This means that we schedule more items for each release than may be
delivered, with the recognition that we may only deliver about 70% of what we
aimed for. So any milestone given to an issue is best-case; it might ship later.

## Previous releases

On our [releases page](/releases/) you can find an overview of the most
important features of recent releases and links to the blog posts for each
release.

## Future releases

GitLab releases a new version [every single month on the 22nd]. Note that we
often move things around, do things that are not listed, and cancel things that
_are_ listed.

This page is always in draft, meaning some of the things here might not ever be
in GitLab. New Starter, Premium, and Ultimate features are indicated with
labels. This is our best estimate of where new features will land, but is in no
way definitive.

The list is an outline of **tentpole features** -- the most important features
of upcoming releases -- and doesn't include most contributions from volunteers
outside the company. This is not an authoritative list of upcoming releases - it
only reflects current [milestones](https://gitlab.com/groups/gitlab-org/milestones).

<%= direction %>

## Paid tiers

### Starter

Starter features are available to anyone with an Enterprise Edition subscription (Starter, Premium, Ultimate).

<%= wishlist["GitLab Starter"] %>

### Premium

Premium features are only available to Premium (and Ultimate) subscribers.

<%= wishlist["GitLab Premium"] %>

### Ultimate

Ultimate is for organisations that have a need to build secure, compliant
software and that want to gain visibility of - and be able to influence - their
entire organisation from a high level. Ultimate features are only be available
to Ultimate subscribers.

<%= wishlist["GitLab Ultimate"] %>

[every single month on the 22nd]: /2015/12/07/why-we-shift-objectives-and-not-release-dates-at-gitlab

## Moonshots

Moonshots are big hairy audacious goals that may take a long time to deliver.

<%= wishlist["moonshots"] %>

## Scope

[Our vision](#vision) is to replace disparate DevOps toolchains with a single integrated application that is pre-configured to work by default across the entire DevOps lifecycle. Inside our scope are the [9 stages of the DevOps lifecycle](/stages-devops-lifecycle/). Consider viewing [the presentation of our plan for 2018](/2017/10/11/from-dev-to-devops/).

We try to prevent maintaining functionality that is language or platform specific because they slow down our ability to get results. Examples of how we handle it instead are:

1. We don't make native mobile clients, we make sure our mobile web pages are great.
1. We don't make native clients for desktop operating systems, people can use [Tower](https://www.git-tower.com/mac/) and for example GitLab was the first to have merge conflict resolution in our web applications.
1. For language translations we [rely on the wider community](https://docs.gitlab.com/ee/development/i18n/translation.html).
1. For Static Application Security Testing we rely on [open source security scanners](https://docs.gitlab.com/ee/ci/examples/sast.html#supported-languages-and-frameworks).
1. For code navigation we're hesitant to introduce navigation improvements that only work for a subset of languages.
1. For [code quality](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality_diff.html) we reuse Codeclimate Engines.
1. For building and testing with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/) we use Heroku Buildpacks.

Outside our scope are Kubernetes and everything it depends on:

1. **Network** (fabric) [Flannel](https://github.com/coreos/flannel/), Openflow, VMware NSX, Cisco ACI
1. **Proxy** (layer 7) [Envoy](https://envoyproxy.github.io/), [nginx](https://nginx.org/en/), [HAProxy](http://www.haproxy.org/), [traefik](https://traefik.io/)
1. **Ingress** [(north/south)](https://networkengineering.stackexchange.com/a/18877) [Contour](https://github.com/heptio/contour), [Ambassador](https://www.getambassador.io/),
1. **Service mesh** [(east/west)](https://networkengineering.stackexchange.com/a/18877) [Istio](https://istio.io/), [Linkerd](https://linkerd.io/)
1. **Container Scheduler** we mainly focus on Kubernetes, other container schedulers are: CloudFoundry, OpenStack, OpenShift, Mesos DCOS, Docker Swarm, Atlas/Terraform, [Nomad](https://nomadproject.io/), [Deis](http://deis.io/), [Convox](http://www.convox.com/), [Flynn](https://flynn.io/), [Tutum](https://www.tutum.co/), [GiantSwarm](https://giantswarm.io/), [Rancher](https://github.com/rancher/rancher/blob/master/README.md)
1. **Package manager** [Helm](https://github.com/kubernetes/helm), [ksonnet](http://ksonnet.heptio.com/)
1. **Operating System** Ubuntu, CentOS, [RHEL](https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux), [CoreOS](https://coreos.com/), [Alpine Linux](https://alpinelinux.org/about/)

Also outside our scope are products that are not specific to developing, securing, or operating applications and digital products.

1. Identity management: Okta and Duo, you use this mainly with SaaS applications you don't develop, secure, or operate.
1. SaaS integration: Zapier and IFTTT
1. Ecommerce: Shopify

In scope are things that are not mainly for SaaS applications:

1. Network security, since it overlaps with application security to some extent.
1. Security information and event management (SIEM), since that measures applications and network.
1. Office productivity applications, since ["We believe that all digital products should be open to contributions, from legal documents to movie scripts and from websites to chip designs"](/company/strategy/#why)

## Vision

From development teams to marketing organizations, everyone needs to collaborate on
digital content. Content should be open to suggestions by a wide number of
potential contributors. Open contribution can be achieved by using a mergeable
file format and distributed version control. The vision of GitLab is to **allow
everyone to collaborate on all digital content** so people can cooperate
effectively and achieve better results, faster.

Ideas flow though many stages before they are realized.
An idea originates in a chat discussion, an issue is created, it is planned in a sprint, coded in an IDE, committed to
version control, tested by CI, code reviewed, deployed, monitored, and documented.
Stitching all these stages of the DevOps lifecycle together can be done in many different ways.
You can have a marketplace of
proprietary apps from different suppliers or use a suite of products developed
in isolation. We believe that a
**single application for the DevOps lifecycle based on convention over configuration**
offers a superior user experience. The
advantage can be quoted from the [Wikipedia page for convention over
configuration](https://en.wikipedia.org/wiki/Convention_over_configuration):
"decrease the number of decisions that developers need to make, gaining
simplicity, and not necessarily losing flexibility". In GitLab you only have to
specify unconventional aspects of your workflow.
The happy path is **frictionless from planning to monitoring**.

We admire other convention over configuration tools like [Ruby on
Rails](http://rubyonrails.org/) (that doctrine of which perfectly describe the [value of integrated systems](http://rubyonrails.org/doctrine#integrated-systems)), [Ember](http://emberjs.com/), and
[Heroku](https://www.heroku.com/), and strive to offer the same advantages for a
continuous delivery of software.

Furthermore, Ruby on Rails has been of massive influence to the Ruby community. Uplifting it,
and making it more powerful and useful than ever before, for many more usecases.
We want GitLab to be to Kubernetes, what Rails is to Ruby.

### Product vision

We're doubling down on our product for concurrent DevOps which brings the entire lifecycle into one
application and lets everyone contribute. We are leaning into what our customers have told us they love: our single
application strategy, our pace of iteration, and our deep focus on users.

#### Strategy 
Our strategy going forward can be summed up in four words:
 * [Depth](#depth)
 * [Breadth](#breadth)
 * [Roles](#roles)
 * [Content](#content)

##### Depth

We aim for market leadership in every category we compete in. We're already there with Source Code Management
and Continous Integration, and we'll continue to build best-in-class, lovable features in Project Management, Release
Automation, Security Testing and Value Stream Management. 

##### Breadth

We wouldn't be true to our ambition if we stopped with our current product categories. Across our existing DevOps
stages we'll continue to rapidly expand to new categories. We'll lead the market and create, compete, and win in
new digital experience creation stages as they evolve.

##### Roles

GitLab enables everyone to contribute. Our platform can be used by all roles contributing to digital content.  
We've already built great workflows and dashboards for Developers, Project Managers, Release Managers, 
Security, and Operations professionals; and we plan to continue to expand to more roles. We'll be building
more powerful tools for Executive visibility, User Experience design, Product Management feedback, Marketing
advocacy, and Product Operations insights.

##### Content

We'll continue to deliver and replace the disparate DevOps toolchain for both static content and traditional
web applications. We'll double down on our investment in cloud native apps, but we aren't done there. GitLab
will expand to enable collaboration on new types of digital content. We're targetting complex microservices,
data-science, mobile apps, and serverless functions. 

#### Goal 

We'll pursue this four pillar strategy while keeping our underlying goal front and center. At GitLab we aim to make it 
faster and easier for groups of contributors to bring value to their consumers. 
 * Faster cycle time driving an increased time to innovation
 * Easier workflow driving increased collaboration and productivity

We're able to do this by providing a [single application](/handbook/product/single-application/) that
[plays well with others](#plays-well-with-others) for teams of any size and role with any kind of project,
while giving you [actionable feedback](#actionable-feedback).

### Product vision background and history

* [The 2019 and Beyond Product Vision](/2018/10/01/gitlab-product-vision/)
* [The Product Vision Backstory](/2018/10/09/gitlab-product-vision-back-story/)
* [The 2018 Product Vision](product-vision)

### DevOps stages

There are individual vision pages and roadmaps for each stage in the [DevOps
lifecycle](https://en.wikipedia.org/wiki/DevOps_toolchain) and the two stages
that span the lifecycle. Click on ones of the stages to see the vision for the
stage.

<!-- Image Map Generated by http://www.image-map.net/ -->
<img src="product-vision/devops-loop-and-spans.png" usemap="#image-map" alt="DevOps Lifecycle">

<map name="image-map">
    <area target="" alt="Manage" title="Manage" href="manage/" coords="813,32,1,1" shape="rect">
    <area target="" alt="Plan" title="Plan" href="plan/" coords="189,62,390,170" shape="rect">
    <area target="" alt="Create" title="Create" href="create/" coords="0,66,185,180" shape="rect">
    <area target="" alt="Verify" title="Verify" href="verify/" coords="1,187,200,309" shape="rect">
    <area target="" alt="Package" title="Package" href="package/" coords="209,183,410,303" shape="rect">
    <area target="" alt="Release" title="Release" href="release/" coords="401,63,628,179" shape="rect">
    <area target="" alt="Configure" title="Configure" href="configure/" coords="634,60,818,183" shape="rect">
    <area target="" alt="Monitor" title="Monitor" href="monitor" coords="611,190,813,309" shape="rect">
    <area target="" alt="Secure" title="Secure" href="secure" coords="819,378,3,343" shape="rect">
</map>

![Product Categories](categories.png)

|  Manage |   Plan  |  Create |  Verify | Package | Release | Configure | Monitor |  Secure |
|:-------:|:-------:|:-------:|:-------:|:-------:|:-------:|:---------:|:-------:|:-------:|
| [Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Amanage&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS) | [Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Aplan&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS) | [Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Acreate&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS) | [Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name[]=devops%3Averify&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS) | [Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Apackage&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS) | [Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Arelease&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS) |  [Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Aconfigure&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS)  | [Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Amonitor&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS) | [Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Asecure&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS) |
|  [Vision](/direction/manage) |  [Vision](/direction/plan) |  [Vision](/direction/create) |  [Vision](/direction/verify) |  [Vision](/direction/package) |  [Vision](/direction/release) |   [Vision](/direction/configure)  |  [Vision](/direction/monitor) |  [Vision](/direction/secure) |

In addition to the 9 stages of the DevOps lifecycle, there are three additional
teams.

| Gitter | Gitaly | Geo |
|:------:|:------:|:---:|
| [Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=Gitter&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS) | [Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=Gitaly&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS) | [Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=Geo&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS) |
| [Vision](/direction/gitter) | [Vision](/direction/gitaly) | [Vision](/direction/geo) |

## Topics

### Actionable feedback

Deployments should never be fire and forget. GitLab will give you immediate
feedback on every deployment on any scale. This means that GitLab can tell you
whether performance has improved on the application level, but also whether
business metrics have changed.

Concretely, we can split up monitoring and feedback efforts within GitLab in
three distinct areas: execution (cycle analytics), business and system feedback.

#### Business feedback

With the power of monitoring and an integrated approach, we have the ability to
do amazing things within GitLab. GitLab will be able to automatically test
commits and versions through feature flags and A/B testing.

Business feedback exists on different levels:

* Short term: how does a certain change perform? Choose A/B based on data.
* Medium term: did a particular new feature change conversions, engagement
* Long term: how do larger efforts relate to changes in conversations, engagement, revenue

- [A/B Testing of branches](https://gitlab.com/gitlab-org/gitlab-ee/issues/117)

#### Application feedback

You application should perform well after changes are made. GitLab will be able to
see whether a change is causing errors or performance issues on application level.
Think about:

* Response times of e.g. a backend API
* Error rates and occurrences of new bugs
* Changes in API calls

#### System feedback

We can now go beyond CI and CD. GitLab will able to tell you whether a change
improved performance or stability. Because it will have access to both
historical data on performance and code, it can show you the impact of any
particular change at any time.

System feedback happens over different time windows:

* Immediate: see whether changes influence availability and alert if they do
* Short-medium term: see whether changes influence system metrics and performance
* Medium-Long term: did a particular effort influence system status

- Implemented: [Performance Monitoring](https://docs.gitlab.com/ee/administration/monitoring/performance/introduction.html)
- [Status monitoring and feedback](https://gitlab.com/gitlab-org/gitlab-ce/issues/25555)
- [Feature monitoring](https://gitlab.com/gitlab-org/gitlab-ce/issues/24254)

#### Execution feedback & Cycle Analytics

GitLab is able to speed up cycle time for any project.
To provide feedback on cycle time GitLab will continue to expand cycle
analytics so that it not only shows you what is slow, it’ll help you speed up
with concrete, clickable suggestions.

- [Cycle Speed Suggestions](https://gitlab.com/gitlab-org/gitlab-ce/issues/25281)

### Why cycle time is important

The ability to monitor, visualize and improve upon cycle time (or: time to value) is fundamental
to GitLab's product. A shorter cycle time will allow you to:

- respond to changing needs faster (i.e. skate to where the puck is going to be)
- ship smaller changes
- manage regressions, rollbacks, bugs better, because you're shipping smaller changes
- make more accurate predictions
- focus on improving customer experience, because you're able to respond to their needs faster

When we're adding new capabilities to GitLab, we tend to focus on things that
will reduce the cycle time for our customers. This is why we choose
[convention over configuration](/handbook/product/#convention-over-configuration)
and why we focus on automating the entire software development lifecycle.

All friction of setting up a new project and building the pipeline of tools
you need to ship any kind of software should disappear when using GitLab.

### Plays well with others

We understand that not everyone will use GitLab for everything all the time, especially when first adopting GitLab.
We want you to use more of GitLab because you love that part of GitLab.
GitLab plays well with others, even when you use only one part of GitLab it should be a great experience.

GitLab ships with built-in integrations to many popular applications. We aspire to have the worlds best integrations for Slack, JIRA, and Jenkins.

Many other applications [integrate with GitLab](/partners/integrate/), and we are open to adding new integrations to our [technology partners page](/partners/). New integrations with GitLab can vary in richness and complexity; from a simple webhook, and all the way to a [Project Service](https://docs.gitlab.com/ee/user/project/integrations/project_services.html).

GitLab [welcomes and supports new integrations](/integrations/) to be created to extend collaborations with other products.
GitLab plays well with others by providing APIs for nearly anything you can do within GitLab.
GitLab can be a [provider of authentication](https://docs.gitlab.com/ee/integration/oauth_provider.html) for external applications.
And of course GitLab is open source so people are very welcome to add anything that they are missing.
If you are don't have time to contribute and am a customer we gladly work with you to design the API addition or integration you need.

### Enterprise editions

GitLab comes in 4 editions:
* **Core**: This edition is aimed at solo developers or teams that
do not need advanced enterprise features. It contains a complete stack with all
the tools developers needs to ship software.
* **Starter**: This edition contains features that are more
relevant for organizations that have more than 100 potential users. For example:
	* features for managers (reports, management tools at the group level,...),
	* features targeted at developers that have to work in multi-disciplinary
	teams (merge request approvals,...),
	* integrations with external tools.
* **Premium**: This edition contains features that are more
relevant for organizations that have more than 750 potential users. For example:
	* features for instance administrators
	* features for managers at the instance level (reporting, management tools,
	roles,...)
	* features to help teams that are spread around the world
	* features for people other than developers that help ship software (support,
	QA, legal,...)
* **Ultimate**: This edition contains
features that are more relevant for organizations that have more than 5000
potential users. For example:
	* compliances and certifications,
	* change management.

### ML/AI at GitLab

Machine learning (ML) through neural networks is a really great tool to solve hard to define, dynamic problems.
Right now, GitLab doesn't use any machine learning technologies, but we expect to use them in the near future
for several types of problems:

#### Signal / noise separation

Signal detection is very hard in an noisy environment. GitLab plans to use
ML to warn users of any signals that stand out against the background noise in several features:

- security scans, notifying the user of stand-out warnings or changes
- error rates and log output, allowing you to rollback / automatically rollback a change if the network notices abberant behavior

#### Recommendation engines

Automatically categorizing and labelling is risky. Modern models tend to overfit, e.g. resulting
in issues with too many labels. However, similar models can be used very well in combination
with human interaction in the form of recommendation engines.

- [suggest labels to add to an issue / MR (one click to add)](https://gitlab.com/tromika/gitlab-issues-label-classification)
- suggest a comment based on your behavior
- suggesting approvers for particular code

#### Smart behavior

Because of their great ability to recognize patterns, neural networks are an excellent
tool to help with scaling, and anticipating needs. In GitLab, we can imagine:

- auto scaling applications / CI based on past load performance
- prioritizing parallized builds based on the content of a change

#### Code quality

Similar to [DeepScan](https://deepscan.io/home/).

#### Code navigation

Similar to [Sourcegraph](https://about.sourcegraph.com/).

### Quarterly Objectives and Key Results (OKRs)

To make sure our goals are clearly defined and aligned throughout the organization, we make use of OKR's (Objective Key Results). Our [quarterly Objectives and Key Results](/okrs) are publicly viewable.
